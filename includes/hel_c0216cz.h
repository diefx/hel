/**------------------------------------------------------------------------------------------------
  * @author  Diego Perez
  * @version V1.0.0
  * @date    06-June-2020
  * @brief   Hardware external driver for the Newheaven display NHD-C026CZ-xxx-xxx-xxx family with
  *          spi interface, no miso signal available on this display. spi frequency must be configure
  *          to send each byte every 30us or more.
  *          NOTE: for the moment only polling spi transfer are supported, not rtos dependancy
-------------------------------------------------------------------------------------------------*/
#ifndef __hel_c0216cz_h__
#define __hel_c0216cz_h__
/* Includes ------------------------------------------------------------------------------------*/
/* Global typedef ------------------------------------------------------------------------------*/
typedef struct hel_lcd_t
{
    hal_spi_handle_t   *spi_handler;            /*!< hal spi handler base address              */

    hal_gpio_t         *rst_port;               /*!< rst signal gpio port, hal_gpio_t port     */
    
    uint16_t            rst_pin;                /*!< rst signal pin, _hal_gpio_pin_x           */
    
    hal_gpio_t         *rs_port;                /*!< rs  signal gpio port, hal_gpio_t port     */
    
    uint16_t            rs_pin;                 /*!< rs  signal pin, _hal_gpio_pin_x           */
    
    hal_gpio_t         *cs_port;                /*!< cs  signal gpio port, hal_gpio_t port     */
    
    uint16_t            cs_pin;                 /*!< cs  signal pin, _hal_gpio_pin_x           */
    
    uint8_t             lines;                  /*!< set the number of lines to use of the 
                                                    display, parameter value of  c0216cz_lines */
    
    uint8_t             char_size;              /*!< set the size of the characters to use in 
                                                    display, parameter value of  c0216cz_dots  */
    
    uint8_t             display_function;       /*!< internal register to keep track of the 
                                                    function had been set on the lcd  */
    
    uint8_t             display_mode;           /*!< internal register to keep track of the 
                                                    display mode had been set on the lcd  */
    
    uint8_t             display_control;        /*!< internal register to keep track of the 
                                                    display control options had been set on the lcd  */
}hel_c0216cz_handle_t;
/* Global define -------------------------------------------------------------------------------*/
/** @defgroup c0216cz_lines   lcd number of lines options
  * @{ */
#define _hel_c0216cz_2line                      0x08
#define _hel_c0216cz_1line                      0x00

/** @defgroup c0216cz_dots   character size in dots
  * @{ */

#define _hel_c0216cz_char_5x16_dots              0x04
#define _hel_c0216cz_char_5x8_dots               0x00
/* Global macro --------------------------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------------------------*/
/* Global function prototypes ------------------------------------------------------------------*/
extern void hel_c0216cz_init( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_mspInit( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_setCursor( hel_c0216cz_handle_t *hlcd, uint8_t col, uint8_t row );
extern void hel_c0216cz_command( hel_c0216cz_handle_t *hlcd, uint8_t value );
extern void hel_c0216cz_data( hel_c0216cz_handle_t *hlcd, uint8_t value );
extern void hel_c0216cz_string( hel_c0216cz_handle_t *hlcd, char *str );
extern void hel_c0216cz_clear( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_home( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_display( hel_c0216cz_handle_t *hlcd, uint32_t state );
extern void hel_c0216cz_blink( hel_c0216cz_handle_t *hlcd, uint32_t state );
extern void hel_c0216cz_cursor( hel_c0216cz_handle_t *hlcd, uint32_t state );
extern void hel_c0216cz_displayLeft( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_displayRight( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_leftToRight( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_rightToLeft( hel_c0216cz_handle_t *hlcd );
extern void hel_c0216cz_autoscroll( hel_c0216cz_handle_t *hlcd, uint32_t state );

#endif
