/* Includes -------------------------------------------------------------------------------------*/
#include "hal.h"
#include <string.h>
#include "hel_c0216cz.h"
/* Private typedef ------------------------------------------------------------------------------*/
/* Private define -------------------------------------------------------------------------------*/
#define  _off                               0u

// commands
#define _hel_c0216cz_clear_display          0x01
#define _hel_c0216cz_return_home            0x02
#define _hel_c0216cz_entry_mode_set         0x04
#define _hel_c0216cz_display_control        0x08
#define _hel_c0216cz_cursor_shift           0x10
#define _hel_c0216cz_function_set           0x20
#define _hel_c0216cz_set_cgram_addr         0x40
#define _hel_c0216cz_set_ddram_addr         0x80

// flags for display entry mode
#define _hel_c0216cz_entry_right            0x00
#define _hel_c0216cz_entry_left             0x02
#define _hel_c0216cz_entry_shift_increment  0x01
#define _hel_c0216cz_entry_shift_decrement  0x00

// flags for display on/off control
#define _hel_c0216cz_display_on             0x04
#define _hel_c0216cz_display_off            0x00
#define _hel_c0216cz_cursor_on              0x02
#define _hel_c0216cz_cursor_off             0x00
#define _hel_c0216cz_blink_on               0x01
#define _hel_c0216cz_blink_off              0x00

// flags for display/cursor shift
#define _hel_c0216cz_display_move           0x08
#define _hel_c0216cz_cursor_move            0x00
#define _hel_c0216cz_mode_right             0x04
#define _hel_c0216cz_mode_left              0x00

// flags for function set
#define _hel_c0216cz_8bit_mode              0x10
#define _hel_c0216cz_4bit_mode              0x00
#define _hel_c0216cz_2line                  0x08
#define _hel_c0216cz_1line                  0x00
#define _hel_c0216cz_5x16_dots              0x04
#define _hel_c0216cz_5x8_dots               0x00
#define _hel_c0216cz_extinst                0x01
#define _hel_c0216cz_norminst               0x00
/* Private macro --------------------------------------------------------------------------------*/
/* Private variables ----------------------------------------------------------------------------*/
/* Private function prototypes ------------------------------------------------------------------*/
/* Private functions ----------------------------------------------------------------------------*/

/**------------------------------------------------------------------------------------------------
 * @brief   Initialize the lcd internal controller, the rutine takes around 35ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_init( hel_c0216cz_handle_t *hlcd )
{
    hel_c0216cz_mspInit( hlcd );

    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_set );
    hal_gpio_writePin( hlcd->rst_port, hlcd->rst_pin, _hal_reset );
    hal_delay( 2u );
    hal_gpio_writePin( hlcd->rst_port, hlcd->rst_pin, _hal_set );
    hal_delay( 20 );

    hel_c0216cz_command( hlcd, 0x30 );/*wakeup*/
    hal_delay( 2 );
    hel_c0216cz_command( hlcd, 0x30 );/*wakeup*/
    hel_c0216cz_command( hlcd, 0x30 );/*wakeup*/
    
    /* configure default display functions, two lines, 5x8 charcters*/
    hlcd->display_function = _hel_c0216cz_8bit_mode | hlcd->lines | hlcd->char_size | _hel_c0216cz_extinst;
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_function_set | hlcd->display_function ) );
    
    hel_c0216cz_command( hlcd, 0x14 );/*internaloscfrequency*/
    hel_c0216cz_command( hlcd, 0x56 );/*powercontroll*/
    hel_c0216cz_command( hlcd, 0x6d );/*followercontrol*/
    hel_c0216cz_command( hlcd, 0x70 );/*constrast*/
    
    // reenable shift functions disbling extended instructions
    hlcd->display_function &= ~_hel_c0216cz_extinst;
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_function_set | hlcd->display_function ) );
    
    // turn the display on with no cursor or blinking default
    hlcd->display_control = _hel_c0216cz_display_on | _hel_c0216cz_cursor_off | _hel_c0216cz_blink_off;  
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_display_control | hlcd->display_control ) );
    
    // Initialize to default text direction (for romance languages)
    hlcd->display_mode = _hel_c0216cz_entry_left | _hel_c0216cz_entry_shift_decrement;
    // set the entry mode
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_entry_mode_set | hlcd->display_mode ) );
    hel_c0216cz_command( hlcd, _hel_c0216cz_clear_display );/*clearscreen*/
    hal_delay( 10 );
}


/**------------------------------------------------------------------------------------------------
 * @brief   Extra initializacion for the lcd, this functions needs to be redeclare on the user 
 *          application prefereable on app_msp.c file
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
__weak void hel_c0216cz_mspInit( hel_c0216cz_handle_t *hlcd )
{

}

/**------------------------------------------------------------------------------------------------
 * @brief   start to write characters on a given column and row
 * @param   hlcd: lcd handler 
 * @param   col:  column number, from 0 to 15
 * @param   row:  row number, from o to 1
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_setCursor( hel_c0216cz_handle_t *hlcd, uint8_t col, uint8_t row )
{
    const uint8_t rows[ 2 ]= { 0x00u, 0x40u };
    uint8_t address;
  
    if( row > 1 )
    {
        row = 1;
    }
    address = rows[ row ] + ( col & 0x0F );
    hel_c0216cz_command( hlcd, ( 0x80 | address ) );
}


/**------------------------------------------------------------------------------------------------
 * @brief   Initialize the lcd internal controller, the rutine takes around 35ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_command( hel_c0216cz_handle_t *hlcd, uint8_t value )
{
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_reset );
    hal_gpio_writePin( hlcd->rs_port, hlcd->rs_pin, _hal_reset );
    hal_spi_transmit( hlcd->spi_handler, &value, 1u, 100u );
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_set );
}


/**------------------------------------------------------------------------------------------------
 * @brief   Initialize the lcd internal controller, the rutine takes around 35ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_data( hel_c0216cz_handle_t *hlcd, uint8_t value )
{
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_reset );
    hal_gpio_writePin( hlcd->rs_port, hlcd->rs_pin, _hal_set );
    hal_spi_transmit( hlcd->spi_handler, &value, 1u, 100u );
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_set );
}


/**------------------------------------------------------------------------------------------------
 * @brief   Initialize the lcd internal controller, the rutine takes around 35ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_string( hel_c0216cz_handle_t *hlcd, char *str )
{
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_reset );
    hal_gpio_writePin( hlcd->rs_port, hlcd->rs_pin, _hal_set );
    hal_spi_transmit( hlcd->spi_handler, (uint8_t*)str, strlen( str ), 100u );
    hal_gpio_writePin( hlcd->cs_port, hlcd->cs_pin, _hal_set );
}


/**------------------------------------------------------------------------------------------------
 * @brief   clear display, set cursor position to zero, this command takes 1ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_clear( hel_c0216cz_handle_t *hlcd )
{
    hel_c0216cz_command( hlcd, _hel_c0216cz_clear_display );
}


/**------------------------------------------------------------------------------------------------
 * @brief   set cursor position to zero, this command takes 1ms
 * @param   hlcd: lcd handler 
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_home( hel_c0216cz_handle_t *hlcd )
{
    hel_c0216cz_command( hlcd, _hel_c0216cz_return_home );
}


/**------------------------------------------------------------------------------------------------
 * @brief   turn on/off display
 * @param   hlcd: lcd handler
 * @param   state: on (1) or off (off)
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_display( hel_c0216cz_handle_t *hlcd, uint32_t state )
{
    if( state == _off )
        hlcd->display_control &= ~_hel_c0216cz_display_on;
    else
        hlcd->display_control |= _hel_c0216cz_display_on;
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_display_control | hlcd->display_control ) );
}


/**------------------------------------------------------------------------------------------------
 * @brief   turn on/off blinky cursor
 * @param   hlcd: lcd handlerr
 * @param   state: on (1) or off (off)
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_blink( hel_c0216cz_handle_t *hlcd, uint32_t state )
{
    if( state == _off )
        hlcd->display_control &= ~_hel_c0216cz_blink_on;
    else
        hlcd->display_control |= _hel_c0216cz_blink_on;
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_display_control | hlcd->display_control ) );
}    
    

/**------------------------------------------------------------------------------------------------
 * @brief   turn on/off cursor
 * @param   hlcd: lcd handler
 * @param   state: on (1) or off (off)
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_cursor( hel_c0216cz_handle_t *hlcd, uint32_t state )
{
    if( state == _off )
        hlcd->display_control &= ~_hel_c0216cz_cursor_on;
    else
        hlcd->display_control |= _hel_c0216cz_cursor_on;
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_display_control | hlcd->display_control ) );
}


/**------------------------------------------------------------------------------------------------
 * @brief   scroll display to left
 * @param   hlcd: lcd handler
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_displayLeft( hel_c0216cz_handle_t *hlcd )
{
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_cursor_shift | _hel_c0216cz_cursor_move | _hel_c0216cz_mode_left ) );
}


/**------------------------------------------------------------------------------------------------
 * @brief   scroll display to right
 * @param   hlcd: lcd handler
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_displayRight( hel_c0216cz_handle_t *hlcd )
{
    hel_c0216cz_command( hlcd, ( _hel_c0216cz_cursor_shift | _hel_c0216cz_cursor_move | _hel_c0216cz_mode_right ) );  
}


/**------------------------------------------------------------------------------------------------
 * @brief   scroll display to right
 * @param   hlcd: lcd handler
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_leftToRight( hel_c0216cz_handle_t *hlcd )
{
    hlcd->display_mode |= _hel_c0216cz_entry_right;
    command( _hel_c0216cz_entry_mode_set | hlcd->display_mode );
}

/**------------------------------------------------------------------------------------------------
 * @brief   scroll display to right
 * @param   hlcd: lcd handler
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_rightToLeft( hel_c0216cz_handle_t *hlcd )
{
    hlcd->display_mode &= ~_hel_c0216cz_entry_left;
    command( _hel_c0216cz_entry_mode_set | hlcd->display_mode );  
}

/**------------------------------------------------------------------------------------------------
 * @brief   scroll display to right
 * @param   hlcd: lcd handler
 * ----------------------------------------------------------------------------------------------*/
void hel_c0216cz_autoscroll( hel_c0216cz_handle_t *hlcd, uint32_t state )
{
    if( state == _off )
        hlcd->display_mode &= ~_hel_c0216cz_entry_shift_increment;
    else
        hlcd->display_mode |= _hel_c0216cz_entry_shift_increment;
    command( _hel_c0216cz_entry_mode_set | hlcd->display_mode );
}
